import React from 'react';
import Banner from '../components/Banner'





export default function NotFound(){

	console.log()

	let bannerComponent = {
		title: "No Page Found",
		description: "E-commerce website for all the Weapons and Items",
		buttonCallToAction: "Go Back to Home",
		destination: "/"
	};


	return (
		
		<Banner bannerProp={bannerComponent}/>

	)
		
}