import React, {useState,useEffect,useContext} from 'react';
import {Form,Button} from 'react-bootstrap';

// import sweetalert2

import Swal from 'sweetalert2';
import UserContext from '../userContext'
import {Redirect,useHistory} from 'react-router-dom'

export default function Register(){

	const {user} = useContext(UserContext);

	// useHistory allows to use methods which will redirect a user to another page without having to reload 

	const history = useHistory();

	/*
		Review 

		Props - is a way to pass data from a parent component to a child component. It is used like an HTML attribute added to the child component. The prop then becomes a property of the special react that all components receive, the props. Props names are user defined 

		States - are a way to store information within a component. This information can be updated within the component. When a state is updated through its setter function, it will re-render the component. They are independent from other instances of the component. 

		Hooks - special/react-defined methods and functions that allows us to do certain tasks in our components.

		useState() - is a hook that creates states. useState() returns an array with 2 items. The first item in the array is the state and the second one is the setter Function. We then destructure this returned array and assign both items in variables: 

		const [stateName, setterFunction] = useState(initial value of the state);

		useEffect() - is a hook that is used to create effects. These effects will allow us to run a function or task based on when our effect will run. Our useEffect() will ALWAYS run on initial render. The next time the useEffect() will run will depend on its dependency array. 

		useEffect(()=>{},[dependencyArray])

		-will allow us to run a function or task on initial render and whenever our component re-renders if there is no dependency array.

		-will allow us to run a function or task on initial render Only if there is an empty dependency array.

		-useEffect will allow us to run a function or task on initial render and whenever the state in it's dependency array is updated. 
	*/


	// input states
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	// conditional rendering for button 
	const [isActive, setIsActive] = useState(false);

	/*
		Two way binding 

		In ReactJS, we are able to create forms which will allow us to bind the value of our input as the value for our states. We cannot type into our inputs anymore because there is now a value bound to it. We will then add an onChange event per input to be able to update the state with the current value of the input. 

		- is done so that we can ensure that we can save the value of our input in our states. So that we can capture the current value of the input as it is typed on and save in a state as opposed to saving it when we are about to submit the values.

		Syntax of Input 

		<Form.Control type="inputType" value={inputState} onChange={e => {setInputState(e.target.value)}}/> 

		e = event, all event listeners pass the event object to the function added in the event listener.

		e.target = target is the element where the event  happened. 

		e.target.value = value is a property of target. It is the current value of the element where the event happened
	*/

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== ""  && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	},[firstName,lastName,email,password,mobileNo,confirmPassword])

	function registerUser(e){
		e.preventDefault();
		// console.log(firstName);
		// console.log(lastName);
		// console.log(email);
		// console.log(mobileNo);
		// console.log(password);
		// console.log(confirmPassword);

		fetch('http://localhost:4000/users/',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			// email property will not be undefined if registered properly
			if(data.email){
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					text: `Thank you for registering, ${data.email}!`
				})

				// redirect our user to our login page
				// push method redirects to another page
				history.push('/login')
				
			}else{
				Swal.fire({
					icon: "error",
					title: "Registration Failure!",
					text: data.message
				})
			}

		})
	}

	return (

			user.id
		?
		<Redirect to="/courses"/>
		:
		<>
			<Form onSubmit={e => registerUser(e)}>
				<h1 className="my-5 text-center">Register</h1>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" value={firstName} onChange={e => {setFirstName(e.target.value)}} placeholder="First Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" value={lastName} onChange={e => {setLastName(e.target.value)}}placeholder="Last Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" value={email} onChange={e => {setEmail(e.target.value)}} placeholder="123@email.com" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}placeholder="11 digit number" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}}placeholder="Enter Password" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} placeholder="Confirm Password" required/>
				</Form.Group>
				{
					isActive
					? <Button className="btn-block mb-3" variant="primary" type="submit">Register</Button>
					: <Button className="btn-block mb-3" variant="secondary" disabled>Register</Button>
				}
				
			</Form>

		  </>
		)

	}	