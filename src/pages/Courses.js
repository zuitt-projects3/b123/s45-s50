import React,{useState,useEffect} from 'react';
// import mock data
import coursesData from '../data/coursesData';
// import course component
import Course from '../components/Course';
export default function Courses(){

	// create an initial value with an empty array
	const [coursesArray,setCoursesArray] = useState([]);

	// console.log(coursesData);

	useEffect(() =>{

		fetch('http://localhost:4000/courses/getActiveCourses')
		.then(res => res.json())
		.then(data => {
		

		// resulting new array from mapping the data (which is an array of course documents) will be set into our courseArray state with its setter function
			setCoursesArray(data.map(course =>{


		

			return (


				<Course key={course._id} courseProp={course}/>
			)
	
		
		}))


		})

	},[])

	console.log(coursesArray);
	/*
		Each instance of a component is independent from one another.  

		So if for example, we called multiple course components, each of this instances are independent from each other. Therefore allowing us to have re-usable UI components.
	*/


	// let coursesCards = coursesData

	
	return (

		
		<>

			<h1 className="my-5 text-center">Available Courses</h1>
			{coursesArray}

		</>

	)
}