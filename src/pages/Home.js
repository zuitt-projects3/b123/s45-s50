import React from 'react'; 
import Banner from '../components/Banner';
import Highlights from '../components/Highlights'; 





/*
	Home will be a page component, which will be our pages for our application. 

	ReactJS adheres to D.R.Y - Don't Repeat Yourself.

	Props - are data we can pass from a parent component to a child component.

	All omponents actually can receive an object, Props are special react objects with which we can pass data around from a parent to child.
*/

export default function Home(){

	let bannerComponent = {
		title: "Weapons and Items Booking System",
		description: "E-commerce website for all the weapons and items",
		buttonCallToAction: "Shop Now!",
		destination: "/login"
	};


	/*
		We can pas props from parent to child by adding HTML-like attributes which we can name ourselves. The name of the attribute will become the property of the object received by all components.

		Note: Components are independent from each other
	*/
	return (

		<>
			<Banner bannerProp={bannerComponent}/> 


			<Highlights /> 

		</>

	)
}
