import React,{useContext,useEffect} from 'react';
import UserContext from '../userContext'

import Banner from '../components/Banner'
export default function Logout(){
	
	const{setUser,unsetUser} = useContext(UserContext);

	// console.log(setUser);
	// console.log(unsetUser);

	unsetUser();

	useEffect(()=>{
		
		setUser({
			id: null,
			isAdmin: null
		})

	},[])

	const bannerComponent = {
		title: "See You Later!",
		description: "You have logged out of Weapons and Items Shop",
		buttonCallToAction: "Go Back to Home Page",
		destination: "/"
	}

	return (

		<Banner bannerProp={bannerComponent}/>

	)
}