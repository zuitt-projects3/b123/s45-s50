import React,{useState,useEffect,useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

import Banner from '../components/Banner';
import {Redirect,useHistory} from 'react-router-dom';


import UserContext from '../userContext';



export default function AddCourse(){

	const{user} = useContext(UserContext)

	const [name,setName] = useState("");
	const [description,setDescription] = useState("")
	const [price,setPrice] = useState(0);

	const [isActive,setIsActive] = useState(false);

	const history = useHistory();


	useEffect(() =>{


		if(name !== "" && description !== "" && price !== 0){
			setIsActive(true);
		} else {
			setIsActive(false);
		}


	},[name,description,price])

	
		

	function createCourse(e){

		e.preventDefault(); 

		


		fetch('http://localhost:4000/courses/',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Cannot Add Course",
					text: data.message
					
				})
								
			
			} else {
				Swal.fire({
					icon: "success",
					title: "Course Added Successfully!",
					text: "Course was Added"
			})
			history.push('/courses')
		}
	})


}
	


	
		


	return (

		user.isAdmin !== true
		?
		<Redirect to="/"/>
		:
		<>
			<h1 className="my-5 text-center">Add Course</h1>
			<Form onSubmit={e => createCourse(e)}>
				
				
				<Form.Group>
					<Form.Label>Name:</Form.Label>
					<Form.Control type="text" value={name} onChange={e => {setName(e.target.value)}} placeholder="Sample Name" required/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}}placeholder="Description" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price:</Form.Label>
					<Form.Control type="number" value={price} onChange={e => {setPrice(e.target.value)}}placeholder="PHP 0.00" required/>
				</Form.Group>
				
				{
					isActive
					? <Button className="btn-block mb-3" variant="primary" type="submit">Submit</Button>
					: <Button className="btn-block" variant="secondary" disabled>Submit</Button>
				}
				
			</Form>

			</>


		)
	
}
