import React from 'react';
import ReactDOM from 'react-dom'; 
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css'
/*
  Reactjs - syntax used is JSX.

  JSX - JavaScript + XML, it is an extension of JavaScript that lets us create objects which will be compiled as HTML ELements.

  With JSX, we are able to create JS objects with HTML like syntax
*/

// let element = <h1>My First React App!</h1>
// console.log(element);

/*
  ReactDom.render(<reactElement>,<htmlElementSelectedById>)
*/

// let myName = <h2>Prince Barro</h2>

/*let person = {
  name: "Stephen Strange",
  age: 45,
  job: "Socerer Supreme",
  income: 50000,
  expense: 30000
}
*/
// let sorcererSupreme = <p>My name {person.name}. I am {person.age} old. I work as a {person.job}</p>

// let personDetails = <p>I am {person.name}. I work as a {person.job}. My income is {person.income}. My expense is {person.expense}. My Balance is {person.income - person.expense}</p>

/*let sorcerer = (
  <>
    <p>My name {person.name}. I am {person.age} old. I work as a {person.job}</p>
    <p>I am {person.name}. I work as a {person.job}. My income is {person.income}. My expense is {person.expense}. My Balance is {person.income - person.expense}</p>
  </>
)*/

/*
   Components starts with capital letters
*/
/*const SampleComp = () => {
  return (

      <h1>I am returned by a function.</h1>


    )
}*/

// How are components called? 
// Synatx: <SampleComp /> a self closing tag to call the component


/*
  In Reactjs, we normally render our components in an entry point or in a mother component called App, this is so we can group our components under a single entry point/main component. 

  All other components/pages will be contained in our main component <App />

  React.StrictMode is a built-in react component which is used to be highlight potential problems in the code and in fact allows to provide more information about errors in our code. 
*/

ReactDOM.render(<React.StrictMode>
      <App />
    </React.StrictMode>,
  document.getElementById('root'));
