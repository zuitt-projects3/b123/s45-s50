import React from 'react'; 
import {Row,Col,Button,Jumbotron} from 'react-bootstrap';


//Link component creates an anchor tag, however, it does not use href and instead it will just commit to switching our page component instead of reloading
import {Link} from 'react-router-dom'


/*
	Row and Col are components from our react-bootstrap module. 
	They create div elements with bootstrap classes. 

	react-bootstrap components create react elements with bootstrap classes.
*/

export default function Banner({bannerProp}){

	// console.log(bannerProp);

	return (
		<Row>
			<Col>
				<Jumbotron className="text-center">
					<h1>{bannerProp.title}</h1>
					<p>{bannerProp.description}</p>
					<Link className="btn btn-primary" to={bannerProp.destination}>{bannerProp.buttonCallToAction}</Link>
				</Jumbotron>
			</Col>
		</Row>
	)
}