import React,{useContext} from 'react';
import {Navbar,Nav} from 'react-bootstrap';

import {Link} from 'react-router-dom'
import UserContext from '../userContext'

export default function AppNavBar(){

	// useContext hook will allow us to "unwrap" our context or use our UserContext, get the values passed to it.

	// useContext return an object after unwrapping our context

	

	// Destructure the returned object by useContext and get our global state
	const {user} = useContext(UserContext);
	console.log(user);


	/*
		If your component will have children (such as context or other react element or components), it should have a closing tag. If not, then we stick to our self closing tag


		"as" prop allows components to be created as if they are another component. A component with the as prop will be able to gain access to another component's properties and functionality
	*/



	return (

		<Navbar bg="secondary" expand="lg">
				<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link as={Link} to="/">Home</Nav.Link>
						<Nav.Link as={Link} to="/courses">Courses</Nav.Link>
						
						{

						user.id
						?
							user.isAdmin === true 
							?
							<>
							<Nav.Link as={Link} to="/addCourses">AddCourses</Nav.Link>
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
							:
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						
							
						:
						<>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
						</>
						}
					
							
						
					</Nav>
				</Navbar.Collapse>
			</Navbar>



		)

}
