import React from 'react'; 
import {Row,Col,Card} from 'react-bootstrap';
export default function Highlights(){
	return (

		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Learn from home</h2>
						</Card.Title>
						<Card.Text>
							Pariatur cupidatat dolor in veniam dolor fugiat non sunt dolore consequat tempor amet consequat esse excepteur ut commodo irure.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Dolore dolor qui amet culpa elit fugiat ut cillum velit ut qui tempor ut incididunt occaecat cillum dolore id exercitation ullamco ut et ullamco deserunt sit deserunt esse esse dolor sed.						
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
					<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Aute sit in et eiusmod commodo aliquip quis deserunt sunt minim aute nostrud officia excepteur ullamco consequat tempor in qui in magna anim eiusmod minim veniam commodo proident laboris culpa consectetur est in quis officia ut quis proident ut.						
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}