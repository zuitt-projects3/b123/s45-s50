import React from 'react';

/*
	Create a Context Object 

	A context is a special React object which will allow us to store information within and pass it around our components within the app.

	The context object is a different approach for passing information between components without the need to pass props from component to component.
*/

const UserContext = React.createContext();

/*
	The provider component is what allows other components to consume or use our context. Any component which is not wrapped by our provider will not have acess to the values provided for our context.
*/

export const UserProvider = UserContext.Provider;

export default UserContext;