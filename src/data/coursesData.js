let coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Labore nostrud reprehenderit eiusmod voluptate deserunt adipisicing minim exercitation aliqua sit aliquip ad.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Laborum minim culpa labore ut ad adipisicing esse sed ullamco aliquip.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Irure incididunt ullamco mollit eiusmod dolor consequat minim do nulla ut commodo in commodo.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "Node.js-MERN",
		description: "Velit ad est laboris ex labore consectetur dolor aliquip id incididunt est enim esse minim cupidatat.",
		price: 45000,
		onOffer: false
	}

]

export default coursesData